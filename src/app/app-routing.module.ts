import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateAccountComponent} from './components/account/create-account/create-account.component';
import {FormAccountComponent} from './components/account/form-account/form-account.component';

const routes: Routes = [
  { path: '', redirectTo: 'create', pathMatch: 'full'},
  { path: 'create', component: CreateAccountComponent},
  { path: 'createForm', component: FormAccountComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
