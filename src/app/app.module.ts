import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import {MaterialModule} from './material-module/material.module';
import {CreateAccountComponent } from './components/account/create-account/create-account.component';
import {AppRoutingModule} from './app-routing.module';
import { FormAccountComponent } from './components/account/form-account/form-account.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateAccountComponent,
    FormAccountComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
