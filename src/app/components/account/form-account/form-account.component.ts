import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form-account',
  templateUrl: './form-account.component.html',
  styleUrls: ['./form-account.component.css']
})
export class FormAccountComponent implements OnInit {
  newAccount = new Account(null, 5, null, false);
  constructor() { }

  ngOnInit() {
  }
  valid() {
    if (this.newAccount.numberCi === null || this.newAccount.numberCi < 9999999 ||
     this.newAccount.city === 0 || this.newAccount.cell === null || this.newAccount.cell < 9999999 ||
     this.newAccount.accept === false) {
      return true;
    } else  {
      return false;
    }
  }
}
export class Account {
  constructor(
    public numberCi: number,
    public city: number,
    public cell: number,
    public accept: boolean,
  ) {}
}
